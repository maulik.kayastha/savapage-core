/*
 * This file is part of the SavaPage project <http://savapage.org>.
 * Copyright (c) 2011-2016 Datraverse B.V.
 * Author: Rijk Ravestein.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * For more information, please contact Datraverse B.V. at this
 * address: info@datraverse.com
 */
package org.savapage.ext.smartschool;

/**
 *
 * @author Rijk Ravestein
 *
 */
public final class SmartschoolConstants {

    public static final String DUPLEX_OFF = "off";

    public static final String PAPERSIZE_A3 = "a3";
    public static final String PAPERSIZE_A4 = "a4";

    public static final String RENDER_MODE_GRAYSCALE = "grayscale";

    public static final String XML_ELM_RETURN = "return";
    public static final String XML_ELM_FILENAME = "filename";
    public static final String XML_ELM_FILESIZE = "filesize";
    public static final String XML_ELM_MD5SUM = "md5sum";
    public static final String XML_ELM_URL = "url";

    public static final String XML_ELM_DATA = "data";
    public static final String XML_ELM_PWD = "pwd";
    public static final String XML_ELM_UID = "uid";
    public static final String XML_ELM_XML = "xml";

    /**
     * Prevent public instantiation.
     */
    private SmartschoolConstants() {

    }
}
